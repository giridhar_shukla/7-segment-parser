    var express = require('express'); 
    var app = express(); 
    var bodyParser = require('body-parser');
    var multer = require('multer');
    var path = require('path'),
        fs = require('fs');

    app.use(function(req, res, next) { //allow cross origin requests
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
        res.header("Access-Control-Allow-Origin", "http://localhost");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    /** Serving from the same express Server
    No cors required */
    app.use(express.static('../client'));
    app.use(bodyParser.json());  

    var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, './uploads/');
        },
        filename: function (req, file, cb) {
            //var datetimestamp = Date.now();
            //cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
            cb(null, file.fieldname + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
        }
    });

    var upload = multer({ //multer settings
                    storage: storage
                }).single('file');

    /** API path that will upload the files */
    app.post('/upload', function(req, res) {
        upload(req,res,function(err){
            if(err){
                 res.json({error_code:1,err_desc:err});
                 return;
            }
            fs.readFile(path.resolve(__dirname + '/uploads/file.txt'), 'utf8', processFile);
            res.json({error_code:0,err_desc:null});
        });
    });

    app.listen('3000', function(){
        console.log('running on 3000...');
    });


    function processFile(err, file) {
        if (err) {
            throw new Error(err);
        }
        process(file);
    }


    var lookup = {
        '1EA': 0,
        '120': 1,
        'F2' : 2,
        '1B2': 3,
        '138': 4,
        '19A': 5,
        '1DA': 6,
        '122': 7,
        '1FA': 8,
        '1BA': 9,
        '17A': 'A',
        '1D8': 'B',
        'CA': 'C',
        '1F0': 'D',
        'DA': 'E',
        '5A': 'F',
        490: 0,
        288: 1,
        242: 2,
        434: 3,
        312: 4,
        410: 5,
        474: 6,
        290: 7,
        506: 8,
        442: 9,
        378: 'A',
        472: 'B',
        202: 'C',
        496: 'D',
        218: 'E',
        90: 'F'
    };

    function getLines(file) {
        return file.split('\n');
    }

    function process(file) {
        // process group of 4 lines
        var q = [],
            i, l, curLine,
            lines = getLines(file),
            numbers = [];
        for (i = 0, l = lines.length; i < l; i++) {
            curLine = lines[i];
            if ((curLine = lines[i]) == '') {
                continue;
            }
            q.push(curLine);
            if (q.length == 3) {
                numbers.push(parseNumber(q));
                q = [];
            }
        }
        fs.writeFile(path.resolve(__dirname + '/uploads/output.txt'), numbers.join('\n'), 'utf8', function (e) {
            if (e) throw new Error(e);
        });
    }

    function parseNumber(lines) {
        var numberSegments = collectNumberSegments(lines);
        var numbers = convertNumberSegmentsToNumbers(numberSegments);
        return numbers;
    }

    function parseLines(lines) {
        var mat = [],
            i, l;
        for (i = 0, l = lines.length; i < l; i++) {
            var curLine = lines[i], grouplet;
            grouplet = curLine.match(/[_| ]{3}/g);
            mat.push(grouplet);
        }
        return mat;
    }

    function transpose(matrix) {
        var row, col, i, j, transpose = [];
        row = matrix.length;
        for (i = 0; i < row; i++) {
            col = matrix[i].length;
            for (j = 0; j < col; j++) {
                transpose[j] = transpose[j] || [];
                transpose[j][i] = matrix[i][j];
            }
        }
        return transpose;
    }

    function collectNumberSegments(lines) {
        var numberMatrix = parseLines(lines),
            transposeMat = transpose(numberMatrix);
        return transposeMat.map(function (segment) {
            return segment.join('');
        });
    }

    function parseNumberSegments(segments) {
        var i, l, codes = [];
        for (i = 0, l = segments.length; i < l; i++) {
            codes.push(parseCandidate(segments[i], i));
        }
        return codes;
    }

    function parseCandidate(candidate, idx) {
        var i, code = 0;
        for (i = 0; i <= 8; i++) {
            if (candidate[i] !== ' ') {
                code +=  1 << i;
            }
        }
        // return code.toString(16).toUpperCase();
        return code;
    }

    function mapCodesToNumbers(codes) {
        var numbers = codes.map(function (code) {
            return lookup[code] === void 0 ? '?' : lookup[code];
        });
        return numbers.join('');
    }

    function convertNumberSegmentsToNumbers(numberSegments) {
        var codes = parseNumberSegments(numberSegments);
        return mapCodesToNumbers(codes);
    }
